
CREATE DATABASE if not exists Colmeia
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

use Colmeia;

create table if not exists Users(
user_id int unique not null auto_increment,
email varchar(200) not null unique,
login varchar(50) not null unique,
pass varchar(60) not null,
followers varchar(50),
followings varchar(255),
primary key (user_id)
);

create table if not exists Posts(
post_id int unique not null auto_increment,
post_legend mediumtext not null,
post_like int,
post_comment varchar(255),
user_id int not null,
constraint fk_user_id_post foreign key (user_id) references Users(user_id)
);