import React, { useState } from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import './index.css';

function Login() {

    //variaveis e constates necessarias para funcoes
    const [form, setForm] = useState({});
    const [error, setError] = useState({});
    const [login, setLogin] = useState();
    var token = sessionStorage.getItem("session_token");
    var [authe, setAuthe] = useState('');

    //verifica se esta logado e direciona
    window.onload = () => {
        if (token) {
            axios.get("http://localhost:3333/feed", { headers: { 'x-access-token': token } })
                .then((res) => {
                    if (res.data.login)
                        setAuthe(true);
                    else
                        setAuthe(false);
                });
        }
    }

    //funcao para validar dados enviados do formulario
    function validate() {
        let errors = {};

        if (form["username"] !== undefined) {
            if (form['username'][0] == "" || form['username'][0] == " " || form['username'][0] == null ||
                form['username'][0] == undefined || form['username'][0].length < 3) {
                errors.username = "Nome de usuário inválido! minimo 3 caracteres";
            }
        } else {
            errors.username = "Campo inválido";
        }

        if (form["password"] !== undefined) {
            var passwordMatch = /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%*()_+^&}{:;?.])(?:([0-9a-zA-Z!@#$%;*(){}_+^&])(?!\1)){6,}$/.test(form["password"][0]);
            if (!passwordMatch) {
                errors.password = "Requisito: letra(maiuscula, minusculas), numeros, simbolos e minimo 6 caracteres";
            }
        } else {
            errors.password = "Campo inválido";
        }

        if (errors)
            return errors;
    };

    //funcao que detecta mudancas nos campos do formulario e carrega em um State do react
    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: [event.target.value]
        })
        console.log(form);
    }

    //funcao que pega, trata e envia os dados para API
    async function handleSubmit(event) {

        event.preventDefault();
        var auth; var token;
        var flag = validate(form);

        if (Object.keys(flag).length === 0) {
            var data = {};
            data.username = form["username"][0];
            data.password = form["password"][0];
            await axios.post('http://localhost:3333/login', data)
                .then(res => {
                    setLogin(res.data.message);
                    auth = res.data.auth;
                    token = res.data.token;
                });
            if (auth) {
                sessionStorage.setItem("session_token", token);
                axios.get('http://localhost:3333/feed', { headers: { 'x-access-token': token } });
                window.location.href = "http://localhost:3000/feed";
            }
        } else {
            setError(flag);
        }

    }

    return (
        <>
            {(authe) ? <Redirect to="/feed" /> : (
                <>
                    <div className="section">
                        <div className="form-content">
                            <div className="form_login">
                                {login && <p className="warning">{login}</p>}
                                <form method="post" action="/login" id="form-login" onSubmit={handleSubmit}>
                                    <a href="/" id="return-link">Voltar</a><br /><br />
                                    <label id="lf" htmlFor="username">Usuário</label>
                                    {error.username && <p className="errors">{error.username}</p>}
                                    <input type="text" name="username" id="username" onChange={e => handleChange(e)} placeholder="Seu nome de usuario" />

                                    <label id="lf" htmlFor="password">Senha</label>
                                    {error.password && <p className="errors">{error.password}</p>}
                                    <input type="text" name="password" id="password" onChange={e => handleChange(e)} placeholder="Sua senha secreta" />

                                    <input type="submit" id="submit" value="Logar" />
                                    <br /><br />
                                </form>
                                <br /><br />
                                <span class="link-register"><Link to="/register">Não possui uma conta?</Link></span>
                            </div>
                        </div>
                    </div>
                </>
            )}
        </>
    );
}

export default Login;