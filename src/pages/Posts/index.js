import React, { useState } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import './index.css';

function Posts() {

    const [user, setUser] = useState();
    const [id, setId] = useState();
    const token = sessionStorage.getItem('session_token');
    const [legend, setLegend] = useState();
    var [warning, setWarning] = useState();
    var [error, setError] = useState();

    //verifica se o JWT é valido para continuar
    window.onload = () => {
        if (token) {
            axios.get("http://localhost:3333/feed", { headers: { 'x-access-token': token } })
                .then((res) => {
                    setUser(res.data.login);
                    setId(res.data.id);
                });
        } else {
            setUser(false);
        }
    }

    //detecta mudancas no campo do formulario e guarda no State
    function handleChange(event){
        setLegend( event.target.value );
    }

    //pega dados do formulario, trata e manda para API, dependendo da resposta imprimi avisos na tela
    async function handleSubmit(event){
        event.preventDefault();
        if(legend != '' && legend != null && legend != undefined && legend.length > 3) {
            console.log('enviando...');
            var data = {};
            data.legend = legend;
            data.id = id;
            await axios.post('http://localhost:3333/npost', data, { headers: { 'x-access-token': token } })
                .then((res) => {
                    if(res.data.message){
                        setWarning('Post publicado com sucesso!')
                        setTimeout(() => {
                            window.location.href = "http://localhost:3000/profile";
                        }, 3000);
                    } else {
                        setWarning('Não foi possível publicar');
                    }
                });
        } else {
            setError("Texto inválido")
        }
    }

    return (
        <>
            {user == false ? <Redirect to="/" /> : (
                <>
                    <form action="/npost" method="post" className="posts-form" onSubmit={handleSubmit}>
                    {warning && <p className="warning">{warning}</p>}
                        <a href="profile" id="return-link">Voltar</a><br/><br/><br/>
                        <p>

                            <label for="legend">Sua frase</label>
                            {error && <p className="errors">{error}</p>}
                            <textarea id="legend" name="legend" type="text" cols="31" rows="10" onChange={e => handleChange(e)} placeholder="Digite sua frase..."/>
                        </p>
                        <p>
                            <input type="submit" value="Postar" id="submit" />
                        </p>
                    </form>
                    <script src="https://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript" charset="utf-8"></script>
                    <script src="js/app.js" type="text/javascript" charset="utf-8"></script>

                </>
            )}
        </>
    );
}

export default Posts;