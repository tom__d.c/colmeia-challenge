import React, { useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import './index.css';

function Register() {

    const [form, setForm] = useState({});
    var [errors, setError] = useState({});
    var [register, setRegister] = useState();
    var token = sessionStorage.getItem("session_token");
    var [authe, setAuthe] = useState();

    //averigua a autenticacao e integridade do token
    window.onload = () => {
        if (token) {
            axios.get("http://localhost:3333/feed", { headers: { 'x-access-token': token } })
                .then((res) => {
                    if (res.data.login)
                        setAuthe(true);
                    else
                        setAuthe(false);
                });
        }
    }

    //detect mudancas nos formulari e registra nos States
    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: [event.target.value]
        })
    }

    //funcao para validar os campos do formulario
    function validate() {
        let errors = {};
        if (form["email"] !== undefined) {
            var emailMatch = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(form["email"][0]);
            if (!emailMatch) {
                errors.email = "E-mail com formato inválido";
            }
        } else {
            errors.email = "Campo inválido";
        }

        if (form["username"] !== undefined) {
            if (form['username'][0] == "" || form['username'][0] == " " || form['username'][0] == null ||
                form['username'][0] == undefined || form['username'][0].length < 3) {
                errors.username = "Nome de usuário inválido! minimo 3 caracteres";
            }
        } else {
            errors.username = "Campo inválido";
        }

        if (form["password"] !== undefined) {
            var passwordMatch = /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%*()_+^&}{:;?.])(?:([0-9a-zA-Z!@#$%;*(){}_+^&])(?!\1)){6,}$/.test(form["password"][0]);
            if (!passwordMatch) {
                errors.password = "Requisito: letra(maiuscula, minusculas), numeros, simbolos e minimo 6 caracteres";
            }
        } else {
            errors.password = "Campo inválido";
        }

        if (form["confirmPassword"] !== undefined) {
            var passwordMatch = /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#$%*()_+^&}{:;?.])(?:([0-9a-zA-Z!@#$%;*(){}_+^&])(?!\1)){6,}$/.test(form["confirmPassword"][0]);
            if (!passwordMatch) {
                errors.confirmPassword = "Senha inválida";
            }
            if (form["password"][0] !== form["confirmPassword"][0]) {
                errors.confirmPassword = "As senhas não coincidem";
            }
        } else {
            errors.confirmPassword = "Campo inválido";
        }

        if (errors)
            return errors;

    }

    //funcao que pega, trata e envia os dados para API
    async function handleSubmit(event) {
        let statusCode;
        event.preventDefault();
        var flag = validate(form);
        if (Object.keys(flag).length === 0) {
            var data = {};
            data.email = form["email"][0];
            data.username = form["username"][0];
            data.password = form["password"][0];
            console.log(data);
            await axios.post('http://localhost:3333/register', data)
                .then(res => {
                    statusCode = res.data.message
                    setRegister(res.data.message);
                });
            (statusCode === "Cadastro realizado com sucesso") && setTimeout(() => {
                window.location.href = "http://localhost:3000/login";
            }, 3000);
        } else {
            setError(flag);
        }
    }


    return (
        <>
            {(authe) ? <Redirect to="/feed" /> : (
                <>
                    <div className="section">
                        <div className="form-content">
                            <div className="form_register">
                                <form method="post" onSubmit={handleSubmit} id="form-register" action="/register" encType="multipart/form-data">
                                    {register && <p className="warning">{register}</p>}
                                    <a href="/" id="return-link">Voltar</a><br /><br /><br />

                                    <label htmlFor="">E-mail</label>
                                    {errors.email && <p className="errors">{errors.email}</p>}
                                    <input type="email" name="email" onChange={e => handleChange(e)} id="email" placeholder="Seu e-mail" />

                                    <label htmlFor="username">Usuário</label>
                                    {errors.username && <p className="errors">{errors.username}</p>}
                                    <input type="text" name="username" onChange={e => handleChange(e)} id="username" placeholder="Seu nome de usuário" />

                                    <label htmlFor="password">Senha</label>
                                    {errors.password && <p className="errors">{errors.password}</p>}
                                    <input type="text" name="password" onChange={e => handleChange(e)} id="password" placeholder="Sua senha secreta" />

                                    <label htmlFor="confirmPassword">Confirme a senha</label>
                                    {errors.confirmPassword && <p className="errors">{errors.confirmPassword}</p>}
                                    <input type="text" name="confirmPassword" onChange={e => handleChange(e)} id="confirmPassword" placeholder="Confirme a senha" />

                                    <input type="submit" value="Cadastrar" id="submit" />
                                    <br /><br />

                                </form>
                                <br /><br />
                                <Link to="/login" className="link-register">Ja possui conta?</Link>
                            </div>
                        </div>
                    </div>
                </>
            )}
        </>
    );
}

export default Register;