import axios from 'axios';
import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { FaChartLine } from 'react-icons/fa';
import './index.css';

function Home() {

    //obtem token jwt e cria constante de autenticacao
    const token = sessionStorage.getItem("session_token");
    const [auth, setAuth] = useState('');

    //verifica se o usuario esta logado e redireciona
    window.onload = () => {
        if (token) {
            axios.get("http://localhost:3333/feed", { headers: { 'x-access-token': token } })
                .then((res) => {
                    if (res.data.login)
                        setAuth(true);
                    else
                        setAuth(false)
                });
        }
    }

    return (
        <>
            {auth ? <Redirect to="/feed" /> : (
                <>
                    <div className="home-page">
                        <div className="home-content">
                            <div className="home-text">
                                <h1>Bem-vindo a <br/><FaChartLine id="icon-stonks"></FaChartLine>&nbsp;&nbsp;&nbsp;Stonks</h1>
                            </div>
                            <div className="home-button">
                                <a href="/register"><button className="register-btn">Cadastre-se</button></a>
                                <a href="/login"><button className="login-btn">Logar</button></a>
                            </div>
                        </div>
                    </div>

                </>
            )}
        </>
    );
}

export default Home;