var http = require('http');
var express = require('express');
var bodyParser = require('body-parser')
var cors = require('cors');
const db = require('../db/db');
const jwt = require('jsonwebtoken');
const SECRET = 'psfoc@411';

var app = express()

var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.use(cors())
app.use(jsonParser);
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader("Access-Control-Allow-Headers", "*");
    next();
});

//middleware autenticacao usando JWT
function verifyJWT(req, res, next) {
    const token = req.headers['x-access-token'];
    jwt.verify(token, SECRET, (err, decoded) => {
        if (err) return res.status(401, "http://localhost:3000/");

        req.user_id = decoded.user_id;
        next();
    });
};

//rota inicial
app.get("/", verifyJWT, (req, res, next) => {
    res.json({ message: "mec" });
});

//rota de registro dos usuários
app.post("/register", urlencodedParser, async (req, res) => {
    let result = await db.insertUser(req.body);
    if (result)
        res.json({ message: "Cadastro realizado com sucesso" });
    else
        res.json({ message: "E-mail ou nome ja existente" });

});

//rota para login 
app.post("/login", urlencodedParser, async (req, res) => {
    let result = await db.selectUsers(req.body);

    if (result) {
        let user_id = result;
        const token = jwt.sign({ user_id }, SECRET, { expiresIn: '2h' });
        return res.json({ auth: true, token });
    } else {
        res.json({ message: "Nome de usuário ou senha inválidos" });
        res.status(401).end();
    }

});


// rota do feed principal, verifica se esta loga e retorna o id do usuario
app.get('/feed', verifyJWT, async (req, res) => {
    var user_login = await db.getUserData(req.user_id);
    if (user_login) {
        res.json({ login: user_login, id: req.user_id });
    }

});

//rota para enviar a frase a ser publicada ou atualizada
app.post('/npost', verifyJWT, async (req, res) => {
    if (req.body.edit) {
        const result = db.updatePost(req.body)
        if (result) {
            res.json({ update: true });
            res.end();
        } else {
            res.json({ update: false });
            res.end();
        }
    } else {
        let result = db.insertPost(req.body);
        if (result) {
            res.json({ message: true });
            res.end();
        } else {
            res.json({ message: false });
            res.end();
        }
    }

});

//rota para obter todos os posts
app.get('/get-all-posts', async (req, res) => {
    const result = await db.returnAllPosts();
    res.json({ posts: result });
    res.end();
})


//rota para obter os posts individuais no perfil
app.post('/get-my-posts', async (req, res) => {
    const result = await db.returnOnePosts(req.body);
    if (result) {
        res.json({ posts: result });
        res.end();
    } else {
        res.end();
    }
});

//rota para deletar posts
app.post('/post-delete', verifyJWT, async (req, res) => {
    const result = await db.deletePost(req.body);
    if (result) {
        res.json({ delete: true });
        res.end();
    } else {
        res.json({ delete: false });
        res.end();
    }
});


//instacia o servidor e coloca pra rodar na porta 3333
var server = http.createServer(app);
server.listen(3333);
console.log("Server listening at port 3333...");

