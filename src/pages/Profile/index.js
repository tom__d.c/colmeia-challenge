import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { FaUser, FaCompass, FaSignInAlt, FaTrashAlt, FaEdit, FaThumbsUp, FaRegComment } from 'react-icons/fa';
import { Redirect } from 'react-router-dom';
import './index.css';

function Profile() {

    var [user, setUser] = useState();
    const token = sessionStorage.getItem('session_token');
    console.log('token:   ',token);
    if(token == null)
        window.location.href = "http://localhost:3000/";
    const [posts, setPosts] = useState([]);
    const [userId, setUserId] = useState();
    const [warning, setWarning] = useState('');
    const [postCount, setPostCount] = useState(0);
    var data = {};

    //verifica se esta o JWT é valido obtem todos os posts do usuario
    window.onload = async () => {
        if (token) {
            await axios.get("http://localhost:3333/feed", { headers: { 'x-access-token': token } })
                .then( async (res) => {
                    setUserId(res.data.id)
                    setUser(res.data.login);
                    var date = {};
                    date.id = res.data.id;
                    await axios.post("http://localhost:3333/get-my-posts", date, { headers: { 'x-access-token': token } })
                        .then((res) => {
                            setPostCount(res.data.posts.length);
                            if (res.data.posts.length > 0) {
                                setPosts(res.data.posts);
                            }
                        });
                });

        } else {
            setUser(false);
        }
    }

    //funcao para deslogar a qualquer momento
    function handleLogout() {
        sessionStorage.removeItem("session_token");
        document.location.reload();
    }

    //funcao para deletar o post, dispara ao clicar no icone de deletar
    function handleDelete(pid) {
        let opt = window.confirm('Tem certeza que deseja deletar?');
        let data = {};
        if (opt) {
            data.id = pid;
            data.user_id = userId;
            axios.post('http://localhost:3333/post-delete', data, { headers: { 'x-access-token': token } })
                .then((res) => {
                    if (res.data.delete) {
                        setWarning('Post deletado com sucesso!');
                        setTimeout(() => {
                            let i = 0;
                            if (i < 1) {
                                window.location.reload();
                                i++;
                            }
                        }, 3000);
                    } else {
                        setWarning('Não foi possivel deletar o post');
                    }
                });
        } else {
            return false;
        }
    }

    //funcao para editar o post assim que clicar no icone de editar
    async function handleEdit(pid) {
        let opt = window.confirm('Tem certeza que deseja editar?');
        let legend = prompt("Digite a nova frase ou a correção: ");
        let data = {};
        if (opt) {
            data.post_id = pid;
            data.legend = legend;
            data.user_id = userId;
            data.edit = true;
            await axios.post('http://localhost:3333/npost', data, { headers: { 'x-access-token': token } })
                .then((res) => {
                    console.log(res);
                    if (res.data.update) {
                        setWarning('Post editado com sucesso!');
                        setTimeout(() => {
                            let i = 0;
                            if (i < 1) {
                                window.location.reload();
                                i++;
                            }
                        }, 3000);
                    } else {
                        setWarning('Não foi possivel editar o post');
                    }
                });
        } else {
            return false;
        }
    }

    return (
        <>
            {user === false ? <Redirect to="/" /> : (
                <>
                    <header>
                        <a href="/feed" class="logo">Stonks</a>
                        <nav class="site-nav">
                            <ul className="header-list">
                                <li><a>Olá, {user}</a></li>
                                <a href="/profile"><li><FaUser></FaUser></li></a>
                                <a href="#"><li><FaCompass></FaCompass></li></a>
                                <a href="#" onClick={handleLogout}><li><FaSignInAlt></FaSignInAlt></li></a>
                            </ul>
                        </nav>
                    </header>

                    <header className="header-img">
                        <div class="container">
                            <div class="profile">

                                <div class="profile-user-settings">
                                    <h1 class="profile-user-name">{user}</h1>
                                    <button class="btn profile-settings-btn" aria-label="profile settings"><i class="fas fa-cog" aria-hidden="true"></i></button>
                                    <a href="/npost" id="link-newp"><button class="btn profile-edit-btn waves-effect waves-light btn modal-trigger">Novo Post</button></a>
                                </div>

                                <div class="profile-stats">
                                    <ul>
                                        <li><span class="profile-stat-count">{postCount}</span> posts</li>
                                        <li><span class="profile-stat-count">0</span> followers</li>
                                        <li><span class="profile-stat-count">0</span> following</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </header>

                    <main>

                        <div className="section">

                            {(posts.length > 0) ?
                                <>
                                    <div className="section">
                                        <div className="w">{warning && <p className="warning">{warning}</p>}</div>
                                        {posts.map((post) => (
                                            <div className="content-quote">
                                                {(post.user_id === userId) ? (
                                                    <>
                                                        <button id="edit-btn" className="btn-action" onClick={() => handleDelete(post.post_id)} ><FaTrashAlt></FaTrashAlt></button>
                                                        <button id="del-btn" className="btn-action" onClick={() => handleEdit(post.post_id)} ><FaEdit></FaEdit></button>
                                                    </>
                                                ) : <></>}

                                                <div className="quote">
                                                    <h3>{post.post_legend}</h3>
                                                </div>
                                                <button id="like-btn" className="btn-actionb"><FaThumbsUp></FaThumbsUp></button>
                                                <button id="comment-btn" className="btn-actionb"><FaRegComment></FaRegComment></button>
                                            </div>
                                        ))}
                                    </div>
                                </>
                                : (
                                    <>
                                        <div className="content-quote">
                                            <h1>Você ainda não postou nada...</h1>
                                        </div>
                                    </>
                                )}
                        </div>

                    </main>
                </>
            )}
        </>

    );
}

export default Profile;