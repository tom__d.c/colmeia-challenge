const mysql = require("mysql2/promise");

//funcao base para iniciar a conexão
async function connect() {
    if (global.connectino && global.connection.state !== "disconnected")
        return global.connection;
    const connection = await mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'uzumaki',
        database: 'colmeia'
    });
    console.log("Conectou a base de dados");
    global.connection = connection;
    return connection;

}

//verifica se o email esta sendo usado ou nome
async function verifyUserExists(user) {
    const conn = await connect();
    const sql = 'SELECT * FROM users WHERE email = ? OR login = ?;';
    const values = [user.email, user.username];
    const rows = await conn.query(sql, values, (err) => {
        if (err)
            return false;
    });
    conn.end();

    return rows;
}


//inseri o usuario na base de dados
async function insertUser(user) {
    const conn = await connect();
    let result = await verifyUserExists(user);
    let flag = Object.values(result[0]).length;
    if (!flag) {
        const sql = 'INSERT INTO Users(email, login, pass) VALUES (?, ?, ?);';
        const values = [user.email, user.username, user.password];
        var valor = await conn.query(sql, values);
        conn.end();

        if(valor[0].affectedRows === 1){
            return true;
        };

    } else {
        return false;
    }
}

//seleciona o usuario enviado no login
async function selectUsers(user){
    const conn = await connect();
    const sql = 'SELECT user_id, login, pass FROM users WHERE login = ? AND pass = ?;';
    const values = [user.username, user.password];
    const rows = await conn.query(sql, values, (err) => {
        if(err)
            return false;
    });
    conn.end();
    if(rows[0][0] == undefined){
        return false;
    } else if(rows[0][0].login == user.username && rows[0][0].pass == user.password) {
        return rows[0][0].user_id;
    }       

}

//obtem o nome do usuario para exibição ou verificar se está autenticado
async function getUserData(user_id){
    const conn = await connect();
    const sql = 'SELECT login FROM users WHERE user_id = ?;';
    const values = [user_id];
    const rows = await conn.query(sql, values, (err) => {
        if(err)
            return false;
    });
    conn.end();
    if(rows[0][0] == undefined){
        return false;
    } else {
        return rows[0][0].login;
    }    
}

//inseri um post na base de dados
async function insertPost(user){
    const conn = await connect();
    const sql = 'INSERT INTO Posts(post_legend, user_id) VALUES (?, ?);';
    const values = [user.legend, user.id];
    var rows = await conn.query(sql, values);
    conn.end();
    if(rows[0].affectedRows === 1)
        return true;
    else    
        return false;

}

//retorna todos os posts para o feed principal
async function returnAllPosts(){
    const conn = await connect();
    const sql = 'SELECT Posts.post_id, Posts.post_legend, Posts.user_id, Users.login FROM Posts INNER JOIN Users ON Posts.user_id = Users.user_id;';
    const [rows] = await conn.query(sql);
    conn.end();
    return rows;  
    
}

//deleta um post
async function deletePost(user){
    const conn = await connect();
    const sql = 'DELETE FROM Posts WHERE Posts.post_id = ? AND Posts.user_id = ?;';
    const values = [user.id, user.user_id];
    const result = await conn.query(sql, values);
    conn.end();
    if(result[0].affectedRows > 0)
        return true;
    else    
        return false;
}


//retorna os seus posts no seu perfil individual
async function returnOnePosts(user){
    const conn = await connect();
    const sql = 'SELECT Posts.post_id, Posts.post_legend, Posts.user_id, Users.login FROM Posts INNER JOIN Users ON Posts.user_id = Users.user_id WHERE Posts.user_id = ?;';
    const values = [user.id];
    const [rows] = await conn.query(sql, values, (err) => {
        if(err)
            return false;
    });
    conn.end();
    return rows;  
    
}

//atualiza um post
async function updatePost(user){
    const conn = await connect();
    const sql = 'UPDATE Posts SET post_legend = ? WHERE Posts.user_id = ? AND Posts.post_id = ?;';
    const values = [user.legend, user.user_id, user.post_id];
    const result = await conn.query(sql, values, (err) => {
        if(err)
            return false;
    });
    conn.end();
    var info = result[0].info;
    var re = new RegExp("Changed: 1");
    var matched = info.match(re);
    if(matched == null)
        return false;
    else    
        return true;
}

module.exports = { insertUser, verifyUserExists, selectUsers, getUserData, insertPost, returnAllPosts, deletePost, returnOnePosts, updatePost };
