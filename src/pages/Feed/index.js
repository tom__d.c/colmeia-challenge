import React, { useState, useEffect } from 'react';
import { FaUser, FaCompass, FaSignInAlt, FaTrashAlt, FaEdit, FaThumbsUp, FaRegComment, FaSearch } from 'react-icons/fa';
import { Redirect } from 'react-router-dom';
import './index.css'
import axios from 'axios';

function Feed() {

    const [user, setUser] = useState();
    var token = sessionStorage.getItem('session_token');
    const [posts, setPosts] = useState([]);
    const [userId, setUserId] = useState();
    const [warning, setWarning] = useState('');
    var [error, setError] = useState();
    var data = {};

    //funcao que verifica autenticacao e pega id (executa assim que a pagina terminar de carregar)
    window.onload = async () => {
        if (token) {
            await axios.get("http://localhost:3333/feed", { headers: { 'x-access-token': token } })
                .then((res) => {
                    data.id = res.data.id;
                    setUserId(data.id);
                    setUser(res.data.login);
                });
            getPosts();

        } else {
            setUser(false);
        }
    }

    //funcao para deslogar o user
    function handleLogout() {
        sessionStorage.removeItem("session_token");
        document.location.reload();
    }

    //obtem posts para carregar o feed 
    function getPosts() {
        axios.get("http://localhost:3333/get-all-posts", { headers: { 'x-access-token': token } })
            .then((res) => {
                if (res.data.posts.length > 0) {
                    setPosts(res.data.posts);
                }
            });
    }

    //funcao executada para ap clicar em deletar
    function handleDelete(pid) {
        let opt = window.confirm('Tem certeza que deseja deletar?');
        let data = {};
        if (opt) {
            data.id = pid;
            data.user_id = userId;
            axios.post('http://localhost:3333/post-delete', data, { headers: { 'x-access-token': token } })
                .then((res) => {
                    if (res.data.delete) {
                        setWarning('Post deletado com sucesso!');
                        setTimeout(() => {
                            let i = 0;
                            if (i < 1) {
                                window.location.reload();
                                i++;
                            }
                        }, 3000);
                    } else {
                        setWarning('Não foi possivel deletar o post');
                    }
                });
        } else {
            return false;
        }
    }

    //funcao executada ao clicar em editar
    async function handleEdit(pid) {
        let opt = window.confirm('Tem certeza que deseja editar?');
        let legend = prompt("Digite a nova frase ou a correção: ");
        let data = {};
        if (opt) {
            data.post_id = pid;
            data.legend = legend;
            data.user_id = userId;
            data.edit = true;
            await axios.post('http://localhost:3333/npost', data, { headers: { 'x-access-token': token } })
                .then((res) => {
                    console.log(res);
                    if (res.data.update) {
                        setWarning('Post editado com sucesso!');
                        setTimeout(() => {
                            let i = 0;
                            if (i < 1) {
                                window.location.reload();
                                i++;
                            }
                        }, 3000);
                    } else {
                        setWarning('Não foi possivel editar o post');
                    }
                });
        } else {
            return false;
        }
    }

    return (
        <>

            {(user === false) ? <Redirect to="/" /> : (
                <>
                    <header>
                        <a href="#" class="logo">Stonks</a>
                        <nav class="site-nav">
                            <ul>
                                <li><a href="#">Olá, {user}</a></li>
                                <a href="/profile"><li><FaUser></FaUser></li></a>
                                <a href="#"><li><FaCompass></FaCompass></li></a>
                                <a href="#" onClick={handleLogout}><li><FaSignInAlt></FaSignInAlt></li></a>
                            </ul>
                        </nav>
                    </header>
                    <div className="section">

                        {(posts.length > 0) ?
                            <>
                                <div className="section">
                                    <div className="w">{warning && <p className="warning">{warning}</p>}</div>

                                    <form className="feed_form" method="post" >
                                        <input type="text" name="search-item" placeholder="Pesquise por algo ou alguém" />
                                        <button type="submit"><FaSearch></FaSearch></button>
                                    </form>

                                    {posts.map((post) => (
                                        <div className="content-quote">
                                            {(post.user_id === userId) ? (
                                                <>
                                                    <button id="edit-btn" className="btn-action" onClick={() => handleDelete(post.post_id)}><FaTrashAlt></FaTrashAlt></button>
                                                    <button id="del-btn" className="btn-action" onClick={() => handleEdit(post.post_id)} ><FaEdit></FaEdit></button>
                                                </>
                                            ) : <></>}

                                            <div className="quote">
                                                <h3>{post.post_legend}</h3>
                                            </div>
                                            <button id="like-btn" className="btn-actionb"><FaThumbsUp></FaThumbsUp></button>
                                            <button id="comment-btn" className="btn-actionb"><FaRegComment></FaRegComment></button>
                                        </div>
                                    ))}
                                </div>
                            </>
                            : (
                                <>
                                    <div className="content-quote">
                                        <h1>Você precisa seguir alguém ou postar algo...</h1>
                                    </div>
                                </>
                            )}
                    </div>
                </>
            )}
        </>

    );
}

export default Feed;