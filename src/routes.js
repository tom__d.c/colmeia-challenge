import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Register from './pages/Register/index';
import Login from './pages/Login/index';
import Home from './pages/Home/index';
import Feed from './pages/Feed/index';
import Profile from './pages/Profile/index';
import Posts from './pages/Posts/index';

//arquivo com rotas e componentes do projeto
function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route component={Home} path="/" exact />
                <Route component={Register} path="/register" />
                <Route component={Login} path="/login" />
                <Route component={Feed} path="/feed" />
                <Route component={Profile} path="/profile" />
                <Route component={Posts} path="/npost" />
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;